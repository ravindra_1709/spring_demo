running project :>
Set MAVEN_HOME and JAVA_HOME in environment variables
Download project to local drive.
Inside the project at root level run command --> mvn spring-boot:run
This will start a local server with application listening on port 8181.


Project Description :->
Weather app provides secure access to users for OpenWeather.org API where-in they don't have to
register to OpenWeather.org directly.User Can register with us and will can provide with gated 
access to OpenWeather.org. System is currently simple but in future it can be enhanced to include
multiple services and data from mutiple providers.

User can access our service api ->http://localhost:8181/userinfo/{name} with his name which will create his record in
our system and assign him alias key which is alias to original WeatherOrg key.

Then using this alias-key and name he can access weather of any city in the world.
http://localhost:8181/weatherinfo/bycity?name={name}&key={key1alias}&city={cityname}
Or
http://localhost:8181/weatherinfo/bycity?name={name}&key={key1alias}&city={cityname,Country}

There is in-built constraint in the system which prevents him using our system more then 5 times in hour.For this 
we are creating record in our system which each time he accesses our system with a timestamp. 

Default user in the system:
http://localhost:8181/userinfo/test
{
"id": 3,
"name": "test",
"date": "2021-07-06",
"keyAlias": {
"id": 1,
"keyAlias": "key1alias",
"active": true
}
}

Create user in the system. This will return key for the user to be used below in the weatherinfo 
http://localhost:8181/userinfo/{name}

Access weather for city:
http://localhost:8181/weatherinfo/bycity?name={name}&key={key1alias}&city={sydney}
Or
http://localhost:8181/weatherinfo/bycity?name={name}&key={key1alias}&city={cityname,Country}
Sample Response :
{
"userId": 3,
"cityWeatherReport": {
"id": 1,
"temperature": "304.14",
"city": "mumbai",
"currentTimeStamp": "2021-07-06T08:57:12.769+00:00"
}
}


To run the application :
I have tested it using JDK1.8 and Maven 3.8.1. And it makes use of spring boot 2.5.2 and inmemory H2 database.

Project Design :
Project follows Tier Architecture with clear separation of duty among Controller layer, Service Layer and Persistence Layer.


Sample Requests and responses:
1. Creating new user:
   http://localhost:8181/userinfo/jon
   
   response :
   {
   "id": 7,
   "name": "jon",
   "date": "2021-07-07",
   "keyAlias": {
   "id": 5,
   "keyAlias": "key5alias",
   "active": true
   }
   }
   
2. Accessing weather report for above user :

http://localhost:8181/weatherinfo/bycity?name=jon&key=key5alias&city=sydney

Response :
{
"userId": 7,
"cityWeatherReport": {
"id": 6,
"descriptions": [
"few clouds"
],
"temperature": "278.53",
"city": "sydney",
"currentTimeStamp": "2021-07-06T21:30:02.842+00:00"
}
}

3. More then 5 times attempt to access the system in 1 hour by user:
http://localhost:8181/weatherinfo/bycity?name=test&key=key1alias&city=mumbai
response
{
"timestamp": "2021-07-06T21:27:14.385+00:00",
"status": 423,
"error": "Locked",
"message": " Please retry after some time.  User has already accessed the system for 5 times in last 1 hour",
"path": "/weatherinfo/bycity"
}


4. Get list of users in system :

   http://localhost:8181/userinfo
   [
   {
   "id": 6,
   "name": "test",
   "date": "2021-07-07",
   "keyAlias": {
   "id": 1,
   "keyAlias": "key1alias",
   "active": true
   }
   },
   {
   "id": 7,
   "name": "jon",
   "date": "2021-07-07",
   "keyAlias": {
   "id": 5,
   "keyAlias": "key5alias",
   "active": true
   }
   }
   ]
   
5. List of weather records created by the user:

http://localhost:8181/userreports/7

Response 
[
{
"userId": 7,
"cityWeatherReport": {
"id": 6,
"descriptions": [
"few clouds"
],
"temperature": "278.53",
"city": "sydney",
"currentTimeStamp": "2021-07-06T21:30:02.842+00:00"
}
}
]



