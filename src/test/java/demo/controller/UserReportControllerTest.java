package demo.controller;

import demo.entity.ApiKey;
import demo.entity.CityWeatherReport;
import demo.entity.UserReport;
import demo.entity.UserInfo;
import demo.service.UserReportService;
import demo.service.UserInfoService;
import demo.service.WeatherInfoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

;


@RunWith(SpringRunner.class)
@WebMvcTest
public class UserReportControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserInfoService userInfoService;
    @MockBean
    private WeatherInfoService weatherInfoService;
    @MockBean
    private UserReportService userReportService;

    @Before
    public void init() throws Exception{
        String pattern = "yyyy-MM-dd";
        LocalDate date = LocalDate.of(2021, 01, 01);
        UserInfo u = new UserInfo();
        u.setId(1l);
        u.setName("name1");
        u.setDate(java.sql.Date.valueOf(date));
        u.setKeyAlias((new ApiKey("key1alias", "AGHEHGHJHJGJHGGJHGgg", true)));
        u.getKeyAlias().setId(1l);

        LocalDateTime dateTime = LocalDateTime.of(2021, 01, 01, 03, 36, 59, 129);
        CityWeatherReport report = new CityWeatherReport("300.0", "mumbai",Arrays.asList("few clouds"), Timestamp.valueOf(dateTime));
        report.setId(1l);
        UserReport userReport = new UserReport(u.getId(), report, Timestamp.valueOf(dateTime));
        userReport.setId(1l);
        when(userReportService.findAllUserReportRecords(u.getId())).thenReturn(Arrays.asList(userReport));
    }

    @Test
    public void findAllUserReports() throws Exception{
        this.mockMvc.perform(get("/userreports/1"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"userId\":1,\"cityWeatherReport\":{\"id\":1,\"descriptions\": [ \"few clouds\"],\"temperature\":\"300.0\",\"city\":\"mumbai\",\"currentTimeStamp\":\"2020-12-31T16:36:59.000+00:00\"}}]"));
    }

}
