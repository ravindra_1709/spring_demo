package demo.controller;

import demo.entity.ApiKey;
import demo.entity.UserInfo;
import demo.service.UserReportService;
import demo.service.UserInfoService;
import demo.service.WeatherInfoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest
public class UserInfoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserInfoService userInfoService;

    @MockBean
    private WeatherInfoService weatherInfoService;

    @MockBean
    private UserReportService userReportService;


    @Before
    public void init() throws Exception{
     LocalDate date = LocalDate.of(2021, 01, 01);
     UserInfo userInfo1 = new UserInfo();
     userInfo1.setId(1l);
     userInfo1.setName("name1");
     userInfo1.setDate(java.sql.Date.valueOf(date));
     userInfo1.setKeyAlias((new ApiKey("key1alias",null , true)));
     userInfo1.getKeyAlias().setId(1l);
     UserInfo userInfo2 = new UserInfo();
     userInfo2.setId(2l);
     userInfo2.setName("name2");
     userInfo2.setDate(java.sql.Date.valueOf(date));
     userInfo2.setKeyAlias((new ApiKey("key2alias",null , true)));
     userInfo2.getKeyAlias().setId(2l);
     List<UserInfo> userInfoList = Arrays.asList(userInfo2,userInfo1);
     when(userInfoService.findAllUser()).thenReturn(userInfoList);

    }

   @Test
    public void findAllUsers() throws Exception{

        // when + then
        this.mockMvc.perform(get("/userinfo"))
                .andExpect(status().isOk())
                .andExpect(content().json("[\n" +
                        "    {\n" +
                        "        \"id\": 1,\n" +
                        "        \"name\": \"name1\",\n" +
                        "        \"date\": \"2021-01-01\",\n" +
                        "        \"keyAlias\": {\n" +
                        "            \"id\": 1,\n" +
                        "            \"keyAlias\": \"key1alias\",\n" +
                        "            \"active\": true\n" +
                        "        }\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"id\": 2,\n" +
                        "        \"name\": \"name2\",\n" +
                        "        \"date\": \"2021-01-01\",\n" +
                        "        \"keyAlias\": {\n" +
                        "            \"id\": 2,\n" +
                        "            \"keyAlias\": \"key2alias\",\n" +
                        "            \"active\": true\n" +
                        "        }\n" +
                        "    }\n" +
                        "]"));
    }

}
