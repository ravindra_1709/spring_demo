package demo.service;

import demo.entity.ApiKey;
import demo.entity.UserInfo;
import demo.exception.UserInfoException;
import demo.repo.UserInfoRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserInfoServiceTest {

    private UserInfoRepository userInfoRepository;

    private UserInfoService userInfoService;
    UserInfo userInfo1 = new UserInfo();
    UserInfo userInfo2 = new UserInfo();

    @Before
    public void init() {
        userInfoRepository = mock(UserInfoRepository.class); // this is needed for inititalizytion of mocks, if you use @Mock
        userInfoService = new UserInfoService();
        userInfoService.setUserInfoRepository(userInfoRepository);
        LocalDate date = LocalDate.of(2021, 01, 01);

        userInfo1.setId(1l);
        userInfo1.setName("name1");
        userInfo1.setDate(java.sql.Date.valueOf(date));
        userInfo1.setKeyAlias((new ApiKey("key1alias",null , true)));
        userInfo1.getKeyAlias().setId(1l);

        userInfo2.setId(2l);
        userInfo2.setName("name2");
        userInfo2.setDate(java.sql.Date.valueOf(date));
        userInfo2.setKeyAlias((new ApiKey("key2alias",null , true)));
        userInfo2.getKeyAlias().setId(2l);
        List<UserInfo> userInfoList = Arrays.asList(userInfo2, userInfo1);
        when(userInfoRepository.findByNameAndKeyAlias("name1", "key2alias")).thenReturn(userInfo1);
        when(userInfoRepository.findAll()).thenReturn(userInfoList);
    }

    @Test
    public void findUserWhichExistsInDb()  {
      UserInfo userInfo1 = userInfoService.getUserInfo("name1", "key2alias");
      Assert.assertEquals("name1", userInfo1.getName());
    }


    @Test(expected = UserInfoException.class)
    public void findUserWhichDoesNotExistsInDb() {
        userInfoService.getUserInfo("invalid", "key2alias");
    }

}
