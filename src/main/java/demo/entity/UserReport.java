package demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class UserReport {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long userId;
    @OneToOne
    private CityWeatherReport cityWeatherReport;
    @JsonIgnore
    private Timestamp timestamp;

    public UserReport(){}

    public UserReport(Long userId, CityWeatherReport cityWeatherReport, Timestamp timestamp) {
        this.userId = userId;
        this.cityWeatherReport = cityWeatherReport;
        this.timestamp = timestamp;
    }

    public UserReport(Long userId, CityWeatherReport cityWeatherReport) {
        this.userId = userId;
        this.cityWeatherReport = cityWeatherReport;
    }

    public CityWeatherReport getCityWeatherReport() {
        return cityWeatherReport;
    }

    public void setCityWeatherReport(CityWeatherReport cityWeatherReport) {
        this.cityWeatherReport = cityWeatherReport;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setId(long id) {
        this.id = id;
    }
}
