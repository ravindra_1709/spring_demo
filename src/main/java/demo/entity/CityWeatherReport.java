package demo.entity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class CityWeatherReport {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @ElementCollection
    public List<String> descriptions;
    public String temperature;
    public String city;
    public Timestamp currentTimeStamp;

    public CityWeatherReport() {
    }

    public CityWeatherReport(String temperature, String city) {
        this.temperature = temperature;
        this.city = city;
    }

    public CityWeatherReport(String temperature, String city, List<String> descriptions, Timestamp currentTimeStamp) {
        this.temperature = temperature;
        this.city = city;
        this.currentTimeStamp = currentTimeStamp;
        this.descriptions = descriptions;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Timestamp getCurrentTimeStamp() {
        return currentTimeStamp;
    }

    public void setCurrentTimeStamp(Timestamp date) {
        this.currentTimeStamp = date;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<String> descriptions) {
        this.descriptions = descriptions;
    }
}
