package demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ApiKey {

    private @Id @GeneratedValue Long id;

    private String keyAlias;
    @JsonIgnore
    private String key;

    private Boolean isActive;

    public ApiKey(String keyAlias, String key, Boolean isActive) {
        this.keyAlias = keyAlias;
        this.key = key;
        this.isActive = isActive;
    }
    public ApiKey(){

    }

    public Long getId() {
        return id;
    }

    public String getKeyAlias() {
        return keyAlias;
    }

    public void setKeyAlias(String keyAlias) {
        this.keyAlias = keyAlias;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public void setId(long id) {
        this.id = id;
    }

}
