package demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.sql.Date;

@Entity
public class UserInfo {

    @Id @GeneratedValue
    private Long id;

    private String name;
    @OneToOne
    private ApiKey apiKey;
    private Date date;

    public UserInfo() {
    }

    public UserInfo(String name, ApiKey apiKey,  Date date) {
        this.name = name;
        this.apiKey = apiKey;
        this.date = date;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApiKey getKeyAlias() {
        return apiKey;
    }

    public void setKeyAlias(ApiKey keyAlias) {
        this.apiKey = keyAlias;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
