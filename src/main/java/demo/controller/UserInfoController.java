package demo.controller;

import demo.entity.UserInfo;
import demo.service.UserInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class UserInfoController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    private UserInfoService userInfoService;

    @GetMapping(path = "/userinfo/{name}", produces = "application/json")
    public ResponseEntity<UserInfo> getUserKey(@PathVariable String name) {
        LOGGER.info("Inside method getUserKey");
        if ((name == null) || name.trim().isEmpty()) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Empty string passed as request parameter");
        }
        UserInfo userInfo = userInfoService.createUserByName(name);
        return ResponseEntity.status(HttpStatus.OK).body(userInfo);
    }

    @GetMapping(path = "/userinfo", produces = "application/json")
    public ResponseEntity<List<UserInfo>> getUserList() {
        LOGGER.info("Inside method getUsesList");
        List<UserInfo> userInfoList = userInfoService.findAllUser();
        return ResponseEntity.status(HttpStatus.OK).body(userInfoList);
    }

}
