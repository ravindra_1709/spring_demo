package demo.controller;

import demo.entity.UserReport;
import demo.service.UserReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserReportController {
    private static final Logger LOGGER= LoggerFactory.getLogger(UserReportController.class);

    @Autowired
    private UserReportService userReportService;

    @GetMapping(path="/userreports/{user}")
    public ResponseEntity<List<UserReport>> getUserReportList(@PathVariable Long user)
    {
        LOGGER.info("Inside method getUserReportList");
        List<UserReport> userReports = userReportService.findAllUserReportRecords(user);
        return ResponseEntity.status(HttpStatus.OK).body(userReports);
    }

    @GetMapping(path="/userreports")
    public ResponseEntity<List<UserReport>> getUserReportList()
    {
        LOGGER.info("Inside method getUserReportList");
        List<UserReport> userReports = userReportService.findAllUserReportRecords();
        return ResponseEntity.status(HttpStatus.OK).body(userReports);
    }

}
