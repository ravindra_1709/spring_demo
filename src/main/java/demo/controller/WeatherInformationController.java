package demo.controller;


import demo.entity.UserReport;
import demo.entity.UserInfo;
import demo.exception.UserInfoException;
import demo.exception.WeatherServiceException;
import demo.service.UserInfoService;
import demo.service.WeatherInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.server.ResponseStatusException;


@RestController
@RequestMapping(path = "/weatherinfo")
public class WeatherInformationController {
    private static final Logger LOGGER= LoggerFactory.getLogger(WeatherInformationController.class);

    @Autowired
    private WeatherInfoService weatherInfoService;
    @Autowired
    private UserInfoService userInfoService;

    @ResponseStatus
    @GetMapping(path="/bycity", produces = "application/json")
    public ResponseEntity<UserReport> findWeatherForCity(@RequestParam(value = "name") String name,
                                                             @RequestParam(value = "city") String city,
                                                                 @RequestParam(value = "key") String key) {
        LOGGER.info("Inside method findWeatherForCity");

        if (!StringUtils.hasLength(name) || !StringUtils.hasLength(city) || !StringUtils.hasLength(key)) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Empty string passed as request parameter");
        }

        UserReport userReport = null;
        try {
            UserInfo userInfo =  userInfoService.getUserInfo(name, key);
            if (userInfo == null) {
                LOGGER.debug("User Record Not Found for give user and key"+ name + ": " + key);
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "User Record Not Found for give user and key");
            }
           userReport = weatherInfoService.getCityWeatherReport(userInfo.getId(), city, userInfo.getKeyAlias().getKey());
        } catch (UserInfoException exc) {
            LOGGER.debug("User Record Not Found for give user and key"+ exc.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "User Record Not Found for give user and key", exc);
        } catch (RestClientException exc) {
            LOGGER.debug("Exception in openweather service call "+ exc.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, " Weather service error."+exc.getMessage(), exc);
        }catch (WeatherServiceException exc) {
            LOGGER.debug("Exception in weather service "+ exc.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.LOCKED, " Please retry after some time. "+ exc.getMessage(), exc);
        }catch (Exception exc) {
            LOGGER.debug("Unhandled Exception in service"+ exc.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, " Service Unavailable. Please retry after some time. ", exc);
        }
       return  ResponseEntity.status(HttpStatus.OK).body(userReport);
    }

}
