package demo.init;


import demo.entity.ApiKey;
import demo.entity.UserInfo;
import demo.repo.ApiKeyRepository;
import demo.repo.UserInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Date;
import java.time.LocalDate;

/**
 * Load initail config in Database
 */
@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    /**
     * Below 2 keys is active at OpenWeather.
     */
    @Bean
    CommandLineRunner initDatabase(ApiKeyRepository apiKeyRepository, UserInfoRepository userInfoRepository) {

        return args -> {
            ApiKey key1 = new ApiKey("key1alias", "0d0b900477eb436cc55396bc88835576", true);
            ApiKey key2 = new ApiKey("key2alias", "94efd18c2f022d15e6e73f33a427b959", true);
            ApiKey key3 = new ApiKey("key3alias", "94efd18c2f022d15e6e73f33a427b959", true);
            ApiKey key4 = new ApiKey("key4alias", "94efd18c2f022d15e6e73f33a427b959", true);
            ApiKey key5 = new ApiKey("key5alias", "94efd18c2f022d15e6e73f33a427b959", true);
            log.info("Preloading " + apiKeyRepository.save(key1));
            log.info("Preloading " + apiKeyRepository.save(key2));
            log.info("Preloading " + apiKeyRepository.save(key3));
            log.info("Preloading " + apiKeyRepository.save(key4));
            log.info("Preloading " + apiKeyRepository.save(key5));
            log.info("Preloading " + userInfoRepository.save(new UserInfo("test",key1, new Date(System.currentTimeMillis()))));
        };
    }

}