package demo.service;

import demo.entity.ApiKey;
import demo.entity.UserInfo;
import demo.exception.UserInfoException;
import demo.repo.ApiKeyRepository;
import demo.repo.UserInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserInfoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserInfoService.class);

    @Autowired
    private ApiKeyRepository apiKeyRepository;
    @Autowired
    private UserInfoRepository userInfoRepository;


    protected ApiKey getRandomApiKey() {
        List<ApiKey> apiKeys = apiKeyRepository.findAll();
        int random = (int) (Math.random() * 10);
        ApiKey apiKey = apiKeys.get(random % apiKeys.size());
        return apiKey;
    }

    public UserInfo getUserInfo(String userName, String keyAlias) {
        LOGGER.info(" Inside getUserInfo method");
        UserInfo userInfo = findUser(userName, keyAlias);
        if (userInfo == null) {
            LOGGER.info(" User not found excepton");
            throw new UserInfoException(" Invalid user or key" + userName + "key" + keyAlias);
        }
        return userInfo;
    }

    public UserInfo createUserByName(String userName) {
        LOGGER.info(" Inside createUserByName method");
        UserInfo userInfo = userInfoRepository.findByName(userName);
        if (userInfo != null)
            return userInfo;
        userInfo = new UserInfo(userName, getRandomApiKey(), new java.sql.Date(new java.util.Date().getTime()));
        userInfoRepository.save(userInfo);
        return userInfo;
    }

    protected UserInfo findUser(String userName, String apiKey) {
        LOGGER.info(" Inside findUser method");
        return userInfoRepository.findByNameAndKeyAlias(userName, apiKey);
    }

    public List<UserInfo> findAllUser() {
        LOGGER.info(" Inside findAllUser method");
        return userInfoRepository.findAll();
    }


    public void deleteByName(String name) {
        LOGGER.info(" Inside findAllUser method");
        UserInfo userInfo = userInfoRepository.findByName(name);
        if (userInfo != null)
        userInfoRepository.delete(userInfo);
        else
            throw new UserInfoException(" User not found in Database " + name);
    }

    public ApiKeyRepository getApiKeyRepository() {
        return apiKeyRepository;
    }

    public void setApiKeyRepository(ApiKeyRepository apiKeyRepository) {
        this.apiKeyRepository = apiKeyRepository;
    }

    public UserInfoRepository getUserInfoRepository() {
        return userInfoRepository;
    }

    public void setUserInfoRepository(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

}
