package demo.service;

import demo.entity.CityWeatherReport;
import demo.entity.UserReport;
import demo.repo.UserReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class UserReportService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserReportService.class);

    @Autowired
    private UserReportRepository userReportRepository;


    protected UserReport createRecord(Long userId, CityWeatherReport cReport, Timestamp callTime) {
        LOGGER.info(" Inside createRecord method");
        UserReport userReport = new UserReport(userId, cReport, callTime);
        return userReportRepository.save(userReport);
    }

    protected Integer noOfRecordsAccessedByUserInHour(Long userId ) {
        LOGGER.info(" Inside noOfRecordsAccessedByUserInHour method");
        java.sql.Timestamp now = new Timestamp(new java.util.Date().getTime());
        Timestamp oneHourAgo = new Timestamp(now.getTime() - 60 * 60 * 1000);
        return userReportRepository.findCountOfRecordsAccessInHour(userId, oneHourAgo, now);
    }

    public List<UserReport> findAllUserReportRecords() {
        LOGGER.info(" Inside findAllUserReportRecords method");
        return userReportRepository.findAll();
    }

    public List<UserReport> findAllUserReportRecords(Long id) {
        LOGGER.info(" Inside findAllUserReportRecords method");
        return userReportRepository.findRecordsByUserId(id);
    }

    public UserReportRepository getUserReportRepository() {
        return userReportRepository;
    }

    public void setUserReportRepository(UserReportRepository userReportRepository) {
        this.userReportRepository = userReportRepository;
    }
}
