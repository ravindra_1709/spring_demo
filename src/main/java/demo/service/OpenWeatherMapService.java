package demo.service;

import demo.pojo.ApiResponse;
import demo.entity.CityWeatherReport;
import demo.pojo.Weather;
import demo.repo.CityWeatherReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class OpenWeatherMapService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenWeatherMapService.class);

    @Value("${url}")
    private String url;
    @Autowired
    private CityWeatherReportRepository cityWeatherReportRepository;

    private RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();

    public CityWeatherReport getWeatherReport(String cityName, String accessKey) throws RestClientException {
        LOGGER.info(" Inside getWeatherReport method");
        RestTemplate restTemplate = restTemplateBuilder.build();
        Timestamp callTime = new java.sql.Timestamp(new java.util.Date().getTime());
        ApiResponse serviceResponse = restTemplate.getForObject(url.concat("?q=").concat(cityName).concat("&appid=").concat(accessKey), ApiResponse.class);
        LOGGER.debug(" Weather Response "+serviceResponse);
        return cityWeatherReportRepository.save(new CityWeatherReport(serviceResponse.main.temp + "", cityName, getDescriptionList(serviceResponse.getWeather()) , callTime));
    }

    private List<String> getDescriptionList(List<Weather> weatherResult) {
        List<String> descriptions = new ArrayList<>();
        if (weatherResult != null) {
            weatherResult.forEach( w -> descriptions.add(w.getDescription()));
        }
        return descriptions;
    }

    public RestTemplateBuilder getRestTemplateBuilder() {
        return restTemplateBuilder;
    }

    public void setRestTemplateBuilder(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplateBuilder = restTemplateBuilder;
    }
}
