package demo.service;

import demo.entity.CityWeatherReport;
import demo.entity.UserReport;
import demo.exception.WeatherServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.sql.Timestamp;

@Service
public class WeatherInfoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherInfoService.class);

    @Autowired
    private UserReportService userAccessReportService;
    @Autowired
    private OpenWeatherMapService openWeatherMapService;

    private RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();

    public UserReport getCityWeatherReport(Long userId, String cityName, String accessKey) throws RestClientException {
        LOGGER.info(" Inside getCityWeatherReport method");
        UserReport userReport = null;

        Integer count = userAccessReportService.noOfRecordsAccessedByUserInHour(userId);
        LOGGER.info(" Number of user access in past 1 hour " + count);
        //proceed in case the calls in last 1 hour is <=5
        if (count < 5) {
            CityWeatherReport cReport = openWeatherMapService.getWeatherReport(cityName, accessKey);
            userReport = userAccessReportService.createRecord(userId, cReport, cReport.currentTimeStamp);
        } else {
            LOGGER.debug(" Exception in WeatherInfoService ");
            throw new WeatherServiceException(" User has already accessed the system for 5 times in last 1 hour");
        }
        return userReport;
    }

    public RestTemplateBuilder getRestTemplateBuilder() {
        return restTemplateBuilder;
    }

    public void setRestTemplateBuilder(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplateBuilder = restTemplateBuilder;
    }
}
