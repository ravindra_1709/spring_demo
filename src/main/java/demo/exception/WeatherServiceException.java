package demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class WeatherServiceException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public WeatherServiceException(String message) {
        super(message);
    }
}
