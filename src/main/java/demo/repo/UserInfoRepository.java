package demo.repo;

import demo.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {

    UserInfo findByName(String name);

    @Query("select u from UserInfo u where u.name = ?1 and u.apiKey.keyAlias = ?2 and u.apiKey.isActive= true")
    UserInfo findByNameAndKeyAlias(String name, String keyAlias);
}
