package demo.repo;

import demo.entity.CityWeatherReport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityWeatherReportRepository extends JpaRepository<CityWeatherReport, Long> {

}
