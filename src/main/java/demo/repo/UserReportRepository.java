package demo.repo;

import demo.entity.UserReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.sql.Timestamp;
import java.util.List;


public interface UserReportRepository extends JpaRepository<UserReport, Long> {

    @Query("select count(u) from UserReport u where u.userId = ?1 and u.timestamp between ?2 and ?3")
    Integer  findCountOfRecordsAccessInHour(Long userId, Timestamp starttime, Timestamp endtime) ;

    @Query("select u from UserReport u where u.userId = ?1 and u.timestamp between ?2 and ?3 ")
    List<UserReport> findRecordsAccessInHour(Long userId, Timestamp startTime, Timestamp endTime) ;

    @Query("select u from UserReport u where u.userId = ?1  order by u.timestamp desc")
    List<UserReport> findRecordsByUserId(Long userId) ;


}
